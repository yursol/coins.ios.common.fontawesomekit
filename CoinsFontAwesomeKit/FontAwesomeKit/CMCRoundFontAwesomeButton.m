//
//  CMCRoundFontAwesomeButton.m
//  FeeneyCDR
//
//  Created by Olesia Dolgopolik on 03/03/15.
//  Copyright (c) 2015 COINS. All rights reserved.
//

#import "CMCRoundFontAwesomeButton.h"

@implementation CMCRoundFontAwesomeButton

- (void)awakeFromNib {
    self.borderWidth = 0;
    self.shadowRadius = 3.0;
    self.shadowOffset = CGSizeMake(3.0, 3.0);
    self.shadowColour = [UIColor lightGrayColor];
    self.cornerRadius = self.frame.size.width/2;
    
    self.iconHeight = self.frame.size.height/2;

    [super awakeFromNib];
}

@end
