//
//  CMCFontAwesomeKitButton.h
//  FeeneyCDR
//
//  Created by Olesia Dolgopolik on 10/04/15.
//  Copyright (c) 2015 COINS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMCFontAwesomeKitButton : UIButton

@property (nonatomic, strong) IBInspectable NSString *iconName;
@property (nonatomic, unsafe_unretained) IBInspectable NSInteger iconHeight;
@property (nonatomic, unsafe_unretained) IBInspectable NSInteger faKitType;

@property (nonatomic) IBInspectable NSInteger borderWidth;
@property (nonatomic) IBInspectable UIColor *borderColour;
@property (nonatomic) IBInspectable NSInteger cornerRadius;
@property (nonatomic) IBInspectable UIColor *shadowColour;
@property (nonatomic) IBInspectable CGFloat shadowRadius;
@property (nonatomic) IBInspectable CGSize shadowOffset;

-(void)setBackgroundLayerColor:(UIColor *)backgroundColor;

-(void)reloadButton;

@end
