//
//  CMCFontAwesomeKitImageView.m
//  FeeneyCDR
//
//  Created by Olesia Dolgopolik on 10/04/15.
//  Copyright (c) 2015 COINS. All rights reserved.
//

#import "CMCFontAwesomeKitImageView.h"
#import "FAKFontAwesome.h"
#import "FAKIonIcons.h"
#import "UIView+CMCFontAwesomeKit.h"

@implementation CMCFontAwesomeKitImageView

- (void)awakeFromNib {
    [self reloadImageView];
}

-(void)reloadImageView {
    NSString *targetKey = [[UIView revertAllIconsForType:self.faKitType] objectForKey:self.iconName];
    if (targetKey) {
        FAKIcon *fakIcon = nil;
        if (self.faKitType == CMCFontAwesome) {
            fakIcon = [FAKFontAwesome iconWithCode:targetKey size:self.frame.size.height];
        } else {
            fakIcon = [FAKIonIcons iconWithCode:targetKey size:self.frame.size.height];
        }
        if (self.fakColor) {
            [fakIcon addAttribute:NSForegroundColorAttributeName value:self.fakColor];
        }
        [self setImage:[fakIcon imageWithSize:self.frame.size]];
    }
}


@end
