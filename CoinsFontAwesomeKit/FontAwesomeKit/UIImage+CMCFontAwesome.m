//
//  UIImage+CMCFontAwesome.m
//  Service
//
//  Created by Sireesha on 23/09/2015.
//  Copyright © 2015 COINS. All rights reserved.
//

#import "UIImage+CMCFontAwesome.h"
#import "FAKFontAwesome.h"

@implementation UIImage (CMCFontAwesome)

+(UIImage *)setFontAwesomeIconColor:(UIColor *)color imageName:(FAKFontAwesome *)fontAwesome size:(CGSize)size
{
    [fontAwesome addAttribute:NSForegroundColorAttributeName value:color];
    return [fontAwesome imageWithSize:size];
}

@end
