//
//  UIButton+CMCFontAwesomeIcon.m
//  mCommons
//
//  Created by Sireesha on 02/04/2015.
//  Copyright (c) 2015 COINS. All rights reserved.
//

#import "UIButton+CMCFontAwesomeIcon.h"
#import "FAKFontAwesome.h"

@implementation UIButton (CMCFontAwesomeIcon)

-(void)setFontAwesomeIconColor:(UIColor *)color imageName:(FAKFontAwesome *)fontAwesome isNormalState:(BOOL)isnormal
{
    [fontAwesome addAttribute:NSForegroundColorAttributeName value:color];
    if(isnormal)
        [self setImage:[fontAwesome imageWithSize:self.bounds.size] forState:UIControlStateNormal];
    else
        [self setImage:[fontAwesome imageWithSize:self.bounds.size] forState:UIControlStateSelected];
}

-(void)setFontAwesomeAttributedStringWithColor:(UIColor *)color fontAwesome:(FAKFontAwesome *)fontAwesome isNormalState:(BOOL)isNormal
{
    [self setTitle:nil forState:UIControlStateNormal];
    [fontAwesome addAttribute:NSForegroundColorAttributeName value:color];
    if(isNormal)
        [self setAttributedTitle:fontAwesome.attributedString forState:UIControlStateNormal];
    else
        [self setAttributedTitle:fontAwesome.attributedString forState:UIControlStateSelected];
}

@end
