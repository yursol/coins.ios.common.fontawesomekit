//
//  CMCFontAwesomeKit.h
//  CDR
//
//  Created by Olesia Dolgopolik on 12/10/15.
//  Copyright © 2015 COINS. All rights reserved.
//

#ifndef CMCFontAwesomeKit_h
#define CMCFontAwesomeKit_h

typedef NS_ENUM(NSInteger, CMCFontAwesomeKitType){
    CMCFontAwesome = 0,
    CMCIon = 1,
    CMCFoundationIcons = 2,
    CMCZocial = 3,
    CMCOcticons = 4,
    CMCMaterialIcons = 5,
    CMCMaterialIcons2 = 6
};

#endif
