//
//  UIButton+CMCFontAwesomeIcon.h
//  mCommons
//
//  Created by Sireesha on 02/04/2015.
//  Copyright (c) 2015 COINS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FAKFontAwesome;

@interface UIButton (CMCFontAwesomeIcon)

-(void)setFontAwesomeIconColor:(UIColor *)color imageName:(FAKFontAwesome *)fontAwesome isNormalState:(BOOL)isnormal;
-(void)setFontAwesomeAttributedStringWithColor:(UIColor *)color fontAwesome:(FAKFontAwesome *)fontAwesome isNormalState:(BOOL)isNormal;

@end
