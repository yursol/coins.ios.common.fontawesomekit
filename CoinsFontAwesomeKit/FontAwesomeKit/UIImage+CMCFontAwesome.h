//
//  UIImage+CMCFontAwesome.h
//  Service
//
//  Created by Sireesha on 23/09/2015.
//  Copyright © 2015 COINS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FAKFontAwesome;

@interface UIImage (CMCFontAwesome)
+(UIImage *)setFontAwesomeIconColor:(UIColor *)color imageName:(FAKFontAwesome *)fontAwesome size:(CGSize)size;
@end
