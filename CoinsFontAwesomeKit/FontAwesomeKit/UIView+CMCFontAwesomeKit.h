//
//  UIView+CMCFontAwesomeKit.h
//  Service
//
//  Created by Olesia Dolgopolik on 10/07/15.
//  Copyright (c) 2015 COINS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMCFontAwesomeKit.h"

@interface UIView (CMCFontAwesomeKit)

+(NSDictionary*)revertAllIconsForType:(CMCFontAwesomeKitType)type;

@end
