//
//  CMCFontAwesomeKitImageView.h
//  FeeneyCDR
//
//  Created by Olesia Dolgopolik on 10/04/15.
//  Copyright (c) 2015 COINS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMCFontAwesomeKitImageView : UIImageView

@property (nonatomic, strong) IBInspectable NSString *iconName;
@property (nonatomic, unsafe_unretained) IBInspectable NSInteger faKitType;
@property (nonatomic, unsafe_unretained) IBInspectable UIColor *fakColor;

-(void)reloadImageView;

@end
