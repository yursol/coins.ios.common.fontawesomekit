//
//  CMCRoundFontAwesomeButton.h
//  FeeneyCDR
//
//  Created by Olesia Dolgopolik on 03/03/15.
//  Copyright (c) 2015 COINS. All rights reserved.
//

#import "CMCFontAwesomeKitButton.h"

@interface CMCRoundFontAwesomeButton : CMCFontAwesomeKitButton

@end
