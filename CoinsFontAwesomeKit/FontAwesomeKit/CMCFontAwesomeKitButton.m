//
//  CMCFontAwesomeKitButton.m
//  FeeneyCDR
//
//  Created by Olesia Dolgopolik on 10/04/15.
//  Copyright (c) 2015 COINS. All rights reserved.
//

#import "CMCFontAwesomeKitButton.h"
#import "FAKFontAwesome.h"
#import "FAKIonIcons.h"
#import "UIView+CMCFontAwesomeKit.h"

@interface CMCFontAwesomeKitButton()

@property (nonatomic) CALayer *backgroundLayer;

@end

@implementation CMCFontAwesomeKitButton

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if (self.shadowRadius > 0) {
        self.backgroundLayer.backgroundColor = self.backgroundColor.CGColor;
        self.backgroundColor = [UIColor clearColor];
        self.backgroundLayer.cornerRadius = self.cornerRadius;
    }
    
    [self setTitle:nil forState:UIControlStateNormal];

    [self reloadButton];
}

-(void)reloadButton
{
    NSString *targetKey = [[UIView revertAllIconsForType:self.faKitType] objectForKey:self.iconName];
    if (targetKey) {
        if (self.iconHeight == 0) {
            self.iconHeight = self.frame.size.height;
        }
        
        FAKIcon *fakIcon = nil;
        if (self.faKitType == CMCFontAwesome) {
            fakIcon = [FAKFontAwesome iconWithCode:targetKey size:self.iconHeight];
        } else {
            fakIcon = [FAKIonIcons iconWithCode:targetKey size:self.iconHeight];
        }
        [fakIcon addAttribute:NSForegroundColorAttributeName value:[self titleColorForState:UIControlStateNormal]];
        [self setAttributedTitle:fakIcon.attributedString forState:UIControlStateNormal];
    }
}

-(void)drawRect:(CGRect)rect {
    self.layer.masksToBounds = YES;
    self.layer.borderColor = self.borderColour.CGColor;
    self.layer.borderWidth = (float)self.borderWidth;
    self.layer.cornerRadius = self.cornerRadius;
    
    if (self.shadowRadius > 0) {
        self.layer.masksToBounds = NO;
        self.layer.shadowRadius = self.shadowRadius;
        self.layer.shadowOpacity = 1.0;
        self.layer.shadowOffset = self.shadowOffset;
        self.layer.shadowColor = self.shadowColour.CGColor;
        
        self.backgroundLayer.frame = self.bounds;
        [self.layer insertSublayer:_backgroundLayer atIndex:0];
    }
}

-(CALayer*)backgroundLayer {
    if (!_backgroundLayer) {
        _backgroundLayer = [CALayer layer];
        _backgroundLayer.frame = self.bounds;
        _backgroundLayer.backgroundColor = self.backgroundColor.CGColor;
        _backgroundLayer.masksToBounds = YES;
        _backgroundLayer.cornerRadius = self.cornerRadius;
        [self.layer insertSublayer:_backgroundLayer atIndex:0];
    }
    return _backgroundLayer;
}

-(void)setBackgroundLayerColor:(UIColor *)backgroundColor{
    self.backgroundLayer.backgroundColor = backgroundColor.CGColor;
    
}

@end
