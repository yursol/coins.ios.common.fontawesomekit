//
//  UIView+CMCFontAwesomeKit.m
//  Service
//
//  Created by Olesia Dolgopolik on 10/07/15.
//  Copyright (c) 2015 COINS. All rights reserved.
//

#import "UIView+CMCFontAwesomeKit.h"
#import "FAKFontAwesome.h"
#import "FAKIonIcons.h"

@implementation UIView (CMCFontAwesomeKit)

+(NSDictionary*)revertAllIconsForType:(CMCFontAwesomeKitType)type
{
    static NSMutableDictionary *revertAllFaIcons;
    static NSMutableDictionary *revertAllIonIcons;
    static dispatch_once_t onceTokenFontAwesomeFA;
    static dispatch_once_t onceTokenFontAwesomeIon;
    
    if (type == CMCFontAwesome) {
        dispatch_once(&onceTokenFontAwesomeFA, ^{
            NSDictionary *fontAwesomeIcons = [FAKFontAwesome allIcons];
            revertAllFaIcons = [[NSMutableDictionary alloc] initWithCapacity:[fontAwesomeIcons count]];
            for (NSString *key in [fontAwesomeIcons allKeys]) {
                [revertAllFaIcons setObject:key forKey:[fontAwesomeIcons objectForKey:key]];
            }
        });
        return revertAllFaIcons;
    }
    else if (type == CMCIon) {
        dispatch_once(&onceTokenFontAwesomeIon, ^{
            NSDictionary *fontAwesomeIcons = [FAKIonIcons allIcons];
            revertAllIonIcons = [[NSMutableDictionary alloc] initWithCapacity:[fontAwesomeIcons count]];
            for (NSString *key in [fontAwesomeIcons allKeys]) {
                [revertAllIonIcons setObject:key forKey:[fontAwesomeIcons objectForKey:key]];
            }
        });
        return revertAllIonIcons;
    }
    return nil;
}

@end
